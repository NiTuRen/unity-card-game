﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffingAbility
{
    public BuffingAbility()
    {
        AbilityName = "";
        Trigger = "";
        NumTargets = 0;
        TargetType = "";
    }

    public string AbilityName { get; set; }
    public string Trigger { get; set; }
    public int NumTargets { get; set; }
    public string TargetType { get; set; }
}