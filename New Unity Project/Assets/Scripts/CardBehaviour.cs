﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardBehaviour : MonoBehaviour {

    //Main card attribute of a card
    public Card CardAttribute = new Card();
    //Collection zone original position
    public Vector2 CollectionOriginal2DPos = new Vector2();

    public void ReturnToCardCollectionAreaState()
    {
        //Load new image for card that enter CardCollectionArea
        Image CCACardImageComp = gameObject.GetComponent<Image>();
        CCACardImageComp.sprite = Resources.Load<Sprite>("Images/" + CardAttribute.CardImageFile);

        //Change card size
        RectTransform CardRectTransform = gameObject.GetComponent<RectTransform>();
        CardRectTransform.sizeDelta = new Vector2(142, 197);
    }

    public void ReturnToDeckCollectionAreaState()
    {
        Image CCACardImageComp = gameObject.GetComponent<Image>();
        CCACardImageComp.sprite = Resources.Load<Sprite>("Images/DeckSmall");

        //Change card size
        RectTransform CardRectTransform = gameObject.GetComponent<RectTransform>();
        CardRectTransform.sizeDelta = new Vector2(265, 48);
    }

    public void ReturnToDeckCollectionDropState(string ImageFileName, int NumCardsInDeck, ref GameObject deckAnchor)
    {
        GameObject ActualDeckObject = gameObject.transform.GetChild(0).gameObject;
        Image DCACardImageComp = ActualDeckObject.GetComponent<Image>();
        DCACardImageComp.sprite = Resources.Load<Sprite>("Images/" + ImageFileName);

        //Change card size
        RectTransform DCAMaskRectTransform = gameObject.GetComponent<RectTransform>();
        DCAMaskRectTransform.transform.localScale = new Vector3(1, 1, 1);

        //Set the position of object in the deck collection zone
        Vector2 deckAnchorPos = deckAnchor.transform.position;
        gameObject.transform.position = deckAnchorPos + new Vector2(-200, -48 * NumCardsInDeck);

        //Update the index of the card drop into deck collection
        DropStateBehaviour DropStatebehavior_Comp = gameObject.GetComponent<DropStateBehaviour>();
        DropStatebehavior_Comp.DropIndex = NumCardsInDeck;
    }

	void Start () {

	}
	
	void Update () {
	}
}
