﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CardCollectionZone : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null) return;

        //eventData.pointerDrag is a gameobject that enter collection zone
        //Change the state of card entering Card collection zone
        CardBehaviour CurrentCardBehaviour = eventData.pointerDrag.GetComponent<CardBehaviour>();
        CurrentCardBehaviour.ReturnToCardCollectionAreaState();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
    }

    public void OnDrop(PointerEventData eventData)
    {
        RectTransform CardRectTransform = eventData.pointerDrag.GetComponent<RectTransform>();
        CardRectTransform.position = eventData.pointerDrag.GetComponent<CardBehaviour>().CollectionOriginal2DPos;
    }
}
