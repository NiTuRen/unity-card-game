﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CardDropZone : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler 
{
    ClassCollection DeckCollection = new ClassCollection();
    List<GameObject> GobjDeckList = new List<GameObject>();

    Vector2 ListStartPos = new Vector2();
    public GameObject deckAnchor;

    void Start()
    {
        RectTransform DropZoneTransform = this.GetComponent<RectTransform>();
        ListStartPos = DropZoneTransform.rect.center;
    }
	public void OnPointerEnter (PointerEventData eventData)
    {
        if (eventData.pointerDrag == null) return;

        //eventData.pointerDrag is a gameobject that enter Drop zone
        //Change the state of card entering Deck collection zone
        CardBehaviour CurrentCardBehaviour = eventData.pointerDrag.GetComponent<CardBehaviour>();
        CurrentCardBehaviour.ReturnToDeckCollectionAreaState();
	}

    public void OnPointerExit(PointerEventData eventData)
    {
    }

    public void OnDrop(PointerEventData eventData)
    {
        //Create a new Deck collection card to drop in the deck collection card area
        GameObject CurrentCard = (GameObject)Instantiate(Resources.Load("Prefab/MaskDeckObject"), new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 0), GameObject.Find("DeckCollectionArea").transform);
        CurrentCard.name = "CCACard " + (DeckCollection.CardsList.Count + 1).ToString();

        //change state of a card to DROP STATE in deck collection area
        string CardImageName = eventData.pointerDrag.GetComponent<CardBehaviour>().CardAttribute.CardImageFile;
        CardBehaviour CurrentCardBehaviour = CurrentCard.GetComponent<CardBehaviour>();
        CurrentCardBehaviour.ReturnToDeckCollectionDropState(CardImageName, DeckCollection.CardsList.Count, ref deckAnchor);

        //Add card to deck
        AddCardToDeck(eventData.pointerDrag.GetComponent<CardBehaviour>().CardAttribute, CurrentCard);

        //return Card Collection Area card to original position and state
        CardBehaviour CCACardBehaviour = eventData.pointerDrag.GetComponent<CardBehaviour>();
        CCACardBehaviour.ReturnToCardCollectionAreaState();
        RectTransform CCACardRectTransform = eventData.pointerDrag.GetComponent<RectTransform>();
        CCACardRectTransform.position = CCACardBehaviour.CollectionOriginal2DPos;
    }

    //Bind gameobject and card attribute together and add it into the deck
    public void AddCardToDeck(Card CardAttribute, GameObject Gobj)
    {
        DeckCollection.CardsList.Add(CardAttribute);
        GobjDeckList.Add(Gobj);
    }

    public void DeleteCardInDeckAt(int Idx)
    {
        DeckCollection.CardsList.RemoveAt(Idx);
        GobjDeckList.RemoveAt(Idx);

        //Update drop index of the cards after current selected cards
        for(int i = Idx; i < DeckCollection.CardsList.Count; ++i)
        {
            --GobjDeckList[i].GetComponent<DropStateBehaviour>().DropIndex;
        }
    }
}
