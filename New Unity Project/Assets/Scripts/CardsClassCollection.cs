﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Xml;
using System.Xml.Serialization;
using System.Reflection;

public class Card
{

    public Card()
    {
        AttributeType = "";
        CardID = -1;
        Name = "";
        CardImageFile = "";
        SpritesheetFile = "";
        Clan = "";
        Attack = 0;
        Health = 0;
        Recharge = 0;
        Cost = 0;
        Ability = "";
        Rarity = 0;
        KillReward = 0;
    }

    public Card(Card c)
    {
        AttributeType = c.AttributeType;
        CardID = c.CardID;
        Name = c.Name;
        CardImageFile = c.CardImageFile;
        SpritesheetFile = c.SpritesheetFile;
        Clan = c.Clan;
        Attack = c.Attack;
        Health = c.Health;
        Recharge = c.Recharge;
        Cost = c.Cost;
        Ability = c.Ability;
        Rarity = c.Rarity;
        KillReward = c.KillReward;
    }


    [XmlAttribute("AttributeType")]
    public string AttributeType { get; set; }
    public int CardID { get; set; }
    public string Name { get; set; }
    public string CardImageFile { get; set; }
    public string SpritesheetFile { get; set; }
    public string Clan { get; set; }
    public int Attack { get; set; }
    public int Health { get; set; }
    public int Recharge { get; set; }
    public int Cost { get; set; }
    public string Ability { get; set; }
    public int Rarity { get; set; }
    public int KillReward { get; set; }
}

public class ClassCollection
{
    public ClassCollection()
    {
        Class = "";
        CardsList = new List<Card>();
    }

    public string Class  { get; set; }
    public List<Card> CardsList  { get; set; }
};

