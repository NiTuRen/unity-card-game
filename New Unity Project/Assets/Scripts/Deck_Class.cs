﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static class Constants
{
	public const int CARDSIZE = 40;
	public const int COMMON = 40;
	public const int RARE = 20;
	public const int EPIC = 10;
	public const int LEGENDARY = 5;
	public const int HANDSIZE = 4;
}


//[ System.Serializable ]
//public struct WTCard {
//public class WTCard : Card {
	//public enum Type { Type1 = 0, Type2, Type3, Type4, Type5, Type6, Type7, Type8, Type9, Type10 }
	//public Type type;
	//public int percents;
	//public string cube;
//};

[ System.Serializable ]
public class Deck
{
	public List< Card > williamDeck; 						// List of cards
	public List< Card > playingDeck; 						// List of cards

	public Deck() { williamDeck = new List< Card >(); playingDeck = new List< Card >(); } 	// Default constructor to initialize the list.

	public void Generate()
	{
		

		for( int i = 0; i < 10; i ++ ) {
			//newWTCard.type = ( WTCard.Type )i;
			Card newWTCard = new Card();
			if (i == 0) {
				newWTCard.Name = "1";
				newWTCard.Rarity = Constants.LEGENDARY;
				newWTCard.CardImageFile = "Images/DrBoomImage"; //"WT_Placeholder/Cube1";
			}
			if (i == 1) {
				newWTCard.Name = "2";
				newWTCard.Rarity = Constants.EPIC;
				newWTCard.CardImageFile = "Images/DrBoomImage"; //"WT_Placeholder/Cube1";
			}
			if (i == 2) {
				newWTCard.Name = "3";
				newWTCard.Rarity = Constants.EPIC;
				newWTCard.CardImageFile = "Images/DrBoomImage"; //"WT_Placeholder/Cube1";
			}
			if (i == 3) {
				newWTCard.Name = "4";
				newWTCard.Rarity = Constants.RARE;
				newWTCard.CardImageFile = "Images/DrBoomImage"; //"WT_Placeholder/Cube1";
			}
			if (i == 4) {
				newWTCard.Name = "5";
				newWTCard.Rarity = Constants.RARE;
				newWTCard.CardImageFile = "Images/DrBoomImage"; //"WT_Placeholder/Cube1";
			}
			if (i == 5) {
				newWTCard.Name = "6";
				newWTCard.Rarity = Constants.RARE;
				newWTCard.CardImageFile = "Images/DrBoomImage"; //"WT_Placeholder/Cube1";
			}
			if (i == 6) {
				newWTCard.Name = "7";
				newWTCard.Rarity = Constants.COMMON;
				newWTCard.CardImageFile = "Images/DrBoomImage"; //"WT_Placeholder/Cube1";
			}
			if (i == 7) {
				newWTCard.Name = "8";
				newWTCard.Rarity = Constants.COMMON;
				newWTCard.CardImageFile = "Images/DrBoomImage"; //"WT_Placeholder/Cube1";
			}
			if (i == 8) {
				newWTCard.Name = "9";
				newWTCard.Rarity = Constants.COMMON;
				newWTCard.CardImageFile = "Images/DrBoomImage"; //"WT_Placeholder/Cube1";
			}
			if (i == 9) {
				newWTCard.Name = "10";
				newWTCard.Rarity = Constants.COMMON;
				newWTCard.CardImageFile = "Images/DrBoomImage"; //"WT_Placeholder/Cube1";
			}

			this.williamDeck.Add (newWTCard);

		}
	}



	/*
	public void GenerateDeck ()
	{
		WTCard newWTCard = new WTCard();

		int avgChange = 0;
		for( int i = 0; i < 10; i ++ ) {
			avgChange += this.williamDeck [i].percents;
		}
		for( int j = 0; j < Constants.CARDSIZE; j ++ ) 
		{
			int cardPos = Random.Range(1, avgChange);
			for(int k = 9; k >= 0; --k)
			{
				cardPos -= this.williamDeck[k].percents;
				if(cardPos <= 0)
				{
					newWTCard.type = ( WTCard.Type )k;
					newWTCard.percents = this.williamDeck [k].percents;
					newWTCard.cube = this.williamDeck [k].cube;
					this.playingDeck.Add( newWTCard );
					break;
				}
			}
		}
			
	}
	*/

	public void GenerateDeck2 ()
	{
		
		List< Card > commonList = new List< Card > (); 
		int avgChange = 0;


		for( int i = 0; i < this.williamDeck.Count; i ++ ) {
			avgChange += this.williamDeck [i].Rarity;
		}

		for (int j = 0; j < this.williamDeck.Count; j++) {
			int cardToMake = ((this.williamDeck [j].Rarity * Constants.CARDSIZE) / (avgChange) ) ;
			if (cardToMake <= 0)
				cardToMake = 1;
			for (int k = 0; k < cardToMake; k++) {
				Card newWTCard = new Card();
				//newWTCard.type = ( WTCard.Type )j;
				newWTCard.Name = this.williamDeck [j].Name;
				Debug.Log (newWTCard.Name);
				newWTCard.Rarity = this.williamDeck [j].Rarity;
				//newWTCard.cube = this.williamDeck [j].cube;
				newWTCard.CardImageFile = this.williamDeck [j].CardImageFile;
				this.playingDeck.Add( newWTCard );
			}

			if(this.williamDeck [j].Rarity == Constants.COMMON)
			{
				commonList.Add(this.williamDeck [j]);
			}
		}

		int a = 0;

		while (this.playingDeck.Count < Constants.CARDSIZE) {
			Card newWTCard = new Card();
			if (a > commonList.Count)
				a = 0;
			//newWTCard.type = ( WTCard.Type )commonList[a].type;
			newWTCard.Name = this.williamDeck [a].Name;
			Debug.Log (newWTCard.Name);
			newWTCard.Rarity = commonList[a].Rarity;
			//newWTCard.cube = commonList[a].cube;
			newWTCard.CardImageFile = this.williamDeck [a].CardImageFile;
			this.playingDeck.Add( newWTCard );
			++a;
		}

		Debug.Log ("this.playingDeck.Count. " + this.playingDeck.Count);

	}

	public void Shuffle()
	{
		System.Random random = new System.Random();

		for( int i = 0; i < this.playingDeck.Count; i ++ ) {
			int j = random.Next( i, this.playingDeck.Count );
			Card temporary = this.playingDeck[ i ];
			this.playingDeck[ i ] = this.playingDeck[ j ];
			this.playingDeck[ j ] = temporary;
		}
			
	}


	/*public static void Shuffle( Deck deck )
	{
		System.Random random = new System.Random();

		for( int i = 0; i < deck.playingDeck.Count; i ++ ) {
			int j = random.Next( i, deck.playingDeck.Count );
			WTCard temporary = deck.playingDeck[ i ];
			deck.playingDeck[ i ] = deck.playingDeck[ j ];
			deck.playingDeck[ j ] = temporary;
		}

		for( int i = 0; i < deck.playingDeck.Count; i ++ ) {
			Debug.Log ( "SHUFFLED: " + deck.playingDeck [i].Name);
		}
	}*/
};