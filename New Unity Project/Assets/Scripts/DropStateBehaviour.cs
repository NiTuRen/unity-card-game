﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropStateBehaviour : MonoBehaviour 
{
    CardDropZone CardDropZone_Comp;
    public int DropIndex = -1;

    void Start()
    {
        CardDropZone_Comp = GameObject.Find("DeckCollectionArea").GetComponent<CardDropZone>();
    }

    void Update()
    {
    }

    public void DeleteCardFromDropZone()
    {
        if(DropIndex != -1)
        {
            CardDropZone_Comp.DeleteCardInDeckAt(DropIndex);
            Destroy(gameObject);
        }
    }
}
