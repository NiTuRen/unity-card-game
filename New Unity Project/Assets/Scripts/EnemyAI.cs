﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class EnemyAI : Singleton<EnemyAI> {

    public string m_RateSpawnFilename;
    private SpawnPool m_SpawnPool = new SpawnPool();

	// Use this for initialization
	void Start () {
        SpawnPool spawnpool = new SpawnPool();
        spawnpool.SpawnList = LevelSerializer.DeserializeFromCSV<Spawn>("SpawnPool", Application.dataPath + "\\CSVfiles\\" + m_RateSpawnFilename).GenericTypeList;

        int currentlevel = 1; // replace with the current level from game manager

        for (int i = 0; i < spawnpool.SpawnList.Count; ++i)
        {
            Spawn currentSpawn = spawnpool.SpawnList[i];
            if (currentSpawn.Level == currentlevel)
            {
                currentSpawn.SpawnedTurns = currentSpawn.MinTurnsPassed;
                m_SpawnPool.SpawnList.Add(currentSpawn);
            }
        }

	}

    public Spawn ComputeEnemySpawn(bool turnPassed)
    {
        Spawn newSpawn = new Spawn();
        List<Spawn> spawnList = m_SpawnPool.SpawnList;
        int unitIDPriority = -1;
        if(turnPassed)
        {
            string debugstring = "";
            foreach (Spawn element in spawnList)
            {
                if (element.SpawnedTurns > 0)
                {
                    element.SpawnedTurns--;
                    // if unit spawn turns just reaches 0, spawn it immediately
                    if (element.SpawnedTurns == 0)
                        unitIDPriority = element.UnitID;
                }
                    
                debugstring += "Unit ID : " + element.UnitID + " Spawned Turns: " + element.SpawnedTurns + " \n";
            }
            Debug.Log(debugstring);
        }

        List<int> randomSpawn = new List<int>();
        for(int i = 0; i < spawnList.Count; ++i)
        {
            // only random those that turn have passed
            if (spawnList[i].SpawnedTurns == 0)
            {
                // to choose between priortise unit id or normal
                int unitIDToSpawn = unitIDPriority == -1 ? spawnList[i].UnitID : unitIDPriority;
                for (int j = 0; j < spawnList[i].SpawnRating; ++j)
                {
                    randomSpawn.Add(unitIDToSpawn);
                }
            }
        }

        // random shuffle the list
        randomSpawn = randomSpawn.OrderBy(x => Random.value).ToList();
        // random a number from list start to end
        int chosenUnitID = randomSpawn[Random.Range(0, randomSpawn.Count())];

        foreach(Spawn element in m_SpawnPool.SpawnList)
        {
            // to random the lane
            if (element.UnitID == chosenUnitID)
            {
                int numLane = element.Lane.Length / 2;
                int chosenLane = Random.Range(0, numLane + 1); // random range max is exclusive, thus the +1
                
                if(chosenLane * 2 <= element.Lane.Length)
                {
                    newSpawn = element;
                    element.SpawnedLane = int.Parse(element.Lane[chosenLane * 2].ToString()); 
                    
                    Debug.Log("Spawned " + element.UnitID + " in lane " + element.Lane[chosenLane * 2]);
                }
                else
                    Debug.Log("Error in " + m_RateSpawnFilename);
                break;
            }
        }
        return newSpawn;
    }
	
	// Update is called once per frame
	void Update () {
        if(Input.GetKeyDown("space"))
            ComputeEnemySpawn(true);
	}
}
