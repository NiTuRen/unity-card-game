﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum STATE
{
    MAINMENU,
    PLAYING
};

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;

    public Camera m_MainCamera;

    public string m_DeckFilename;
    public string m_PlayerDeckFilename;
    public ClassCollection m_Deck;
    private ClassCollection m_PlayerDeck;

    // Awake is always called before any Start functions
	void Awake () 
    {
        if (instance != this)
            instance = this;
        else if (instance != null)
            Destroy(gameObject);
        
        DontDestroyOnLoad(gameObject);
	}

    void Start()
    {
        m_Deck = new ClassCollection();

        // load the deck and all the cards
        m_PlayerDeck = LevelSerializer.DeserializeFromFile<ClassCollection>(Application.dataPath + "\\XmlFiles\\" + m_PlayerDeckFilename);
        m_Deck.CardsList = LevelSerializer.DeserializeFromCSV<Card>("ClassCollection", Application.dataPath + "\\CSVfiles\\" + m_DeckFilename).GenericTypeList;
    }

    public void GoToPlaying()
    {
        SceneManager.LoadScene("InGame");
    }

    public void GoToDeckCollection()
    {
        SceneManager.LoadScene("DeckCollection");
    }

    // saving all players cards before quitting
    void OnApplicationQuit()
    {
        LevelSerializer.SerializeToFile<ClassCollection>(Application.dataPath + "\\XmlFiles\\" + m_PlayerDeckFilename, m_PlayerDeck);
    }
	
	// Update is called once per frame
	void Update () 
    {
        /*
        if (Input.GetKeyDown(KeyCode.Escape))
            SceneManager.LoadScene("MainMenu");
        */
	}
}
