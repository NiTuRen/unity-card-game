﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HandCardBehaviour : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler  
{

	public Transform parentToReturnTo = null;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		Debug.Log("Selected Card Name: " + this.name);
		GetComponent<CanvasGroup>().blocksRaycasts = false;
		this.GetComponent<RectTransform>().sizeDelta = new Vector2(200, 200);
		GameObject.Find ("Main Camera").GetComponent<HandScript> ().selectedCard = this.gameObject;
		this.transform.GetChild (1).GetComponent<Transform> ().position = this.transform.GetChild (6).position;
		this.transform.GetChild (2).GetComponent<Transform> ().position = this.transform.GetChild (7).position;

	}

	public void OnDrag(PointerEventData eventData)
	{
		transform.position = eventData.position;
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		GetComponent<CanvasGroup>().blocksRaycasts = true;
		this.GetComponent<RectTransform>().sizeDelta = new Vector2(150, 150);
		this.transform.GetChild (1).GetComponent<Transform> ().position = this.transform.GetChild (4).position;
		this.transform.GetChild (2).GetComponent<Transform> ().position = this.transform.GetChild (5).position;
		GameObject.Find ("Main Camera").GetComponent<HandScript> ().UpdatePos ();
		GameObject.Find ("Main Camera").GetComponent<HandScript> ().selectedCard = null;
	}

}
