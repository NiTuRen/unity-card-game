﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;

public class HandScript : MonoBehaviour {
	
	public Deck mydeck;
	public GameObject selectedCard;

	bool updateHandPos = false;
	int counter = 0;
	List< GameObject > shuffledDeck; 
	List< GameObject > handWTCard; 
	GameObject mainCam;
	Vector3 camPos;
	GameObject canvas;

	// Use this for initialization
	void Start () {
		shuffledDeck = new List< GameObject > (); 
		handWTCard = new List< GameObject > (); 
		mainCam = GameObject.FindGameObjectWithTag ("MainCamera");
		canvas = GameObject.Find ("Canvas");
		camPos = mainCam.GetComponent<Transform> ().position;
		mydeck = new Deck ();
		mydeck.Generate ();
		mydeck.GenerateDeck2 ();
		//Deck.Shuffle (mydeck);
		mydeck.Shuffle();
		CreateDeck ();
	}


	void CreateDeck()
	{
		for (int i = 0; i < Constants.CARDSIZE; ++i) {
			Debug.Log ("Generated: " + mydeck.playingDeck [counter].Name);
			GameObject t = Instantiate (Resources.Load("Prefab/HandCard"), new Vector3 (camPos.x, camPos.y, camPos.z), Quaternion.Euler(new Vector3(0,0,0))) as GameObject;
			shuffledDeck.Add (t);
			t.GetComponent<Image>().sprite = Resources.Load<Sprite>(mydeck.playingDeck [counter].CardImageFile);
			t.GetComponent<RectTransform>().sizeDelta = new Vector2(150, 150);
			t.transform.GetChild (0).GetComponentInChildren<Text> ().text = mydeck.playingDeck [counter].Name;
			t.transform.GetChild (1).GetComponentInChildren<Text> ().text = (mydeck.playingDeck [counter].Attack).ToString();
			t.transform.GetChild (2).GetComponentInChildren<Text> ().text = (mydeck.playingDeck [counter].Health).ToString();
			t.transform.GetChild (3).GetComponentInChildren<Text> ().text = (mydeck.playingDeck [counter].Cost).ToString();
			t.name = mydeck.playingDeck [counter].Name;
			t.SetActive (false);
			t.transform.SetParent(canvas.transform);
			++counter;
		}
	}
		

	public void UpdatePos()
	{
		for (int i = 0; i < handWTCard.Count; ++i) {
			
			handWTCard [i].GetComponent<HandCardBehaviour> ().parentToReturnTo = GameObject.Find ("Anchor" + i).GetComponent<Transform>();
			handWTCard [i].GetComponent<Transform> ().position = handWTCard [i].GetComponent<HandCardBehaviour> ().parentToReturnTo.position;
		}
	}


	public void RemoveCardFromHand(GameObject cardToRemove)
	{
		Debug.Log ("Played: " + cardToRemove.name);
		//Debug.Log ("Index of the card to remove: " + handWTCard.IndexOf (cardToRemove).ToString ());
		handWTCard.RemoveAt(handWTCard.IndexOf(cardToRemove));
		Destroy (cardToRemove);
		updateHandPos = true;
		selectedCard = null;
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			shuffledDeck [0].SetActive (true);
			handWTCard.Add (shuffledDeck [0]);
			shuffledDeck.RemoveAt (0);
			updateHandPos = true;
			Debug.Log ("shuffledDeck.Count: " + shuffledDeck.Count);
		}

		if (handWTCard.Count > Constants.HANDSIZE) {
			GameObject temp = handWTCard [0];
			Debug.Log ("Thrown: " + temp.name);
			handWTCard.RemoveAt (0);
			Destroy (temp);
			updateHandPos = true;
		}

		if (Input.GetKeyDown (KeyCode.Alpha1)) {
			GameObject temp = handWTCard [0];
			Debug.Log ("Played: " + temp.name);
			handWTCard.RemoveAt (0);
			Destroy (temp);
			updateHandPos = true;
		}

		if (Input.GetKeyDown (KeyCode.Alpha2)) {
			GameObject temp = handWTCard [1];
			Debug.Log ("Played: " + temp.name);
			handWTCard.RemoveAt (1);
			Destroy (temp);
			updateHandPos = true;
		}

		if (Input.GetKeyDown (KeyCode.Alpha3)) {
			GameObject temp = handWTCard [2];
			Debug.Log ("Played: " + temp.name);
			handWTCard.RemoveAt (2);
			Destroy (temp);
			updateHandPos = true;
		}

		if (Input.GetKeyDown (KeyCode.Alpha4)) {
			GameObject temp = handWTCard [3];
			Debug.Log ("Played: " + temp.name);
			handWTCard.RemoveAt (3);
			Destroy (temp);
			updateHandPos = true;
		}

		if(shuffledDeck.Count <= 0)
		{
			Debug.Log ("shuffledDeck.Count: " + shuffledDeck.Count);
			counter = 0;
			mydeck.Shuffle ();
			CreateDeck();
		}

		if (updateHandPos) {
			updateHandPos = false;
			UpdatePos();
		}
	}
}

//at start instantiate all 40 cards
//set them inactive
//only the first 4 is active
//once out of hand, put them in temp deck and inactive them