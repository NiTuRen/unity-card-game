﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LaneDropZone : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler  
{
	public int lane;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	public void OnPointerEnter (PointerEventData eventData)
	{
		if (eventData.pointerDrag == null) return;
	}

	public void OnPointerExit(PointerEventData eventData)
	{
	}

	public void OnDrop(PointerEventData eventData)
	{
		Debug.Log ("Inside Lane: " + lane.ToString ());
		GameObject.Find ("Main Camera").GetComponent<HandScript> ().RemoveCardFromHand (GameObject.Find ("Main Camera").GetComponent<HandScript> ().selectedCard);
	}
}
