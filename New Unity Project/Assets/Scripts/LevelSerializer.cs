﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Reflection;

public class GenericType<T>
{
    public List<T> GenericTypeList { get; set; }
}

public static class LevelSerializer
{
    //Serialize to XML
    public static void SerializeToFile<T>(string path, T obj)
    {
        var serializer = new XmlSerializer(typeof(T));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, obj);
        }
    }
    //DeSerialize from XML
    public static T DeserializeFromFile<T>(string path)
    {
        var serializer = new XmlSerializer(typeof(T));
        using (var stream = new FileStream(path, FileMode.Open))
        {
            return (T)serializer.Deserialize(stream);
        }
    }

    //Set object properties by variable name
    public static void SetObjectProperty<T>(object obj, string propertyName, string StrVal)
    {
        PropertyInfo pi = typeof(T).GetProperty(propertyName);
        string typename = pi.PropertyType.Name;

        if (typename.Contains("Int"))
        {
            int val = 0;
            if (int.TryParse(StrVal, out val))
                pi.SetValue(obj, int.Parse(StrVal), null);
        }
        else if (typename.Contains("Single"))
        {
            float val = 0;
            if (float.TryParse(StrVal, out val))
                pi.SetValue(obj, float.Parse(StrVal), null);
        }
        else if (typename.Contains("Double"))
        {
            double val = 0;
            if (double.TryParse(StrVal, out val))
                pi.SetValue(obj, double.Parse(StrVal), null);
        }
        else
            pi.SetValue(obj, StrVal, null);
    }

    public static GenericType<T> DeserializeFromCSV<T>(string CSVName, string path)
    {
        GenericType<T> NewGenericType = new GenericType<T>();
        NewGenericType.GenericTypeList = new List<T>();

        using (var fs = File.OpenRead(path))
        using (var reader = new StreamReader(fs))
        {
            int iCurrLineNumber = 1;
            string[] Properties = null;
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                string[] values = line.Split(',');
                if (values.Length == 0) return null;

                switch (iCurrLineNumber)
                {
                    case 1:
                        if (values[0] != CSVName) return null;
                        break;
                    case 2:
                        Properties = values;
                        break;
                    default:
                        T instance = (T)Activator.CreateInstance(typeof(T));

                        for (int i = 0; i < Properties.Length; ++i)
                        {
                            SetObjectProperty<T>(instance, Properties[i], values[i]);
                        }

                        NewGenericType.GenericTypeList.Add(instance);
                        break;
                }
                ++iCurrLineNumber;
            }
        }
        return NewGenericType;
    }

    public static void SerializeCardsToCSV(string path, ref ClassCollection RefClassCollection)
    {
        using (var fs = File.OpenWrite(path))
        {
            //fs.wrote
        }
        File.WriteAllText(path, "");
    }
}