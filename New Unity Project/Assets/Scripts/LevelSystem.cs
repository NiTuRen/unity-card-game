﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSystem : MonoBehaviour {

    public GameObject CardCollectionArea;
    public List<Vector2> CardsPlacementPositionList;
    public List<GameObject> AnchorList;

    public Dictionary<int, List<GameObject>> CardList = new Dictionary<int, List<GameObject>>();
    public int CurrentPage = 0;

	// Use this for initialization
	void Start () {
        LoadCards();
	}
	
    void LoadCards()
    {
        CardCollectionArea = GameObject.Find("Canvas");
        //CardsPlacementPositionList = GetCardsAlignmentData(CardCollectionArea);
        CardsPlacementPositionList = GetAnchors();
        /*
        foreach(Vector2 it in CardsPlacementPositionList)
        {
            Debug.Log(it.ToString());
        }
        */

        //InstantiateCardsFromFile("Card", Application.dataPath + "\\XmlFiles\\card.xml", CardCollectionArea, CardsPlacementPositionList);
        InstantiateCardsFromFile("Card", Application.dataPath + "\\CSVfiles\\CardsCollection.csv", CardCollectionArea, CardsPlacementPositionList);
    }

    List<Vector2> GetAnchors()
    {
        List<Vector2> ret = new List<Vector2>();
        foreach(GameObject anchors in AnchorList )
        {
            ret.Add(new Vector2(anchors.transform.position.x, anchors.transform.position.y));
        }

        return ret;
    }

    List<Vector2> GetCardsAlignmentData(GameObject CardCollectionArea)
    {
        List<Vector2> TempPositionList = new List<Vector2>();
        RectTransform CollectionAreaTransform = CardCollectionArea.GetComponent<RectTransform>();
        float ColAreaTotalWidth = CollectionAreaTransform.rect.xMax - CollectionAreaTransform.rect.xMin;
        float ColAreaTotalHeight = CollectionAreaTransform.rect.yMax - CollectionAreaTransform.rect.yMin;
        float MaxCol = 4.0f;
        float MaxRow = 2.0f;
        Vector2 PlacementStep, PlacementOffset;
        PlacementStep.x = ColAreaTotalWidth / MaxCol;
        PlacementStep.y = ColAreaTotalHeight / MaxRow;
        PlacementOffset.x = PlacementStep.x / 2.0f - CollectionAreaTransform.rect.xMin;
        PlacementOffset.y = PlacementStep.y / 2.0f;

        //Find the position of each cards to place in a page
        for (int y = 0; y < (int)MaxRow; ++y)
        {
            for (int x = 1; x <= (int)MaxCol; ++x)
            {
                TempPositionList.Add(new Vector2((PlacementStep.x * x) - PlacementOffset.x, PlacementStep.y - (PlacementStep.y * y) - PlacementOffset.y));
            }
        }
        return TempPositionList;
    }

    void InstantiateCardsFromFile(string prefabName, string filepath, GameObject Parent, List<Vector2> PositionList)
    {
        //ClassCollection Collection = LevelSerializer.DeserializeFromFile<ClassCollection>(filepath);

        ClassCollection Collection = new ClassCollection();
        Collection.CardsList = LevelSerializer.DeserializeFromCSV<Card>("ClassCollection", filepath).GenericTypeList;
        int cardIdx = 0;
        int pageIdx = -1;
        foreach (Card card_ in Collection.CardsList)
        {
            //Instantiate new card
            GameObject CurrentCard = (GameObject)Instantiate(Resources.Load("Prefab/" + prefabName), new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 0), Parent.transform);
            CurrentCard.name = "Card" + (cardIdx + 1).ToString();
            UpdateCardAttribute(CurrentCard, card_);

            //Copy serialize card attribute to current card attribute
            CardBehaviour CurrentCardBehaviour = CurrentCard.GetComponent<CardBehaviour>();
            CurrentCardBehaviour.CardAttribute = new Card(card_);

            //Put the 1st 8 cards into the 1st page
            if (cardIdx < PositionList.Count)
            {
                CurrentCard.transform.position = PositionList[cardIdx];
            }
            //Put the rest of the cards outside of the canvas
            else
            {
                RectTransform ParentRect = Parent.GetComponent<RectTransform>();
                CurrentCard.transform.position = new Vector2(ParentRect.rect.xMax + 1000.0f, ParentRect.rect.yMax + 1000.0f);
            }

            //Save collection zone original position
            CurrentCardBehaviour.CollectionOriginal2DPos = CurrentCard.transform.position;

            //Create new pages to place the cards
            if (cardIdx % PositionList.Count == 0)
            {
                ++pageIdx;
                CardList.Add(pageIdx, new List<GameObject>());
            }

            //Add cards into pages
            CardList[pageIdx].Add(CurrentCard);
            ++cardIdx;
        }
    }

    public void UpdateCardAttribute(GameObject CurrentCard, Card CardInfo)
    {
        CurrentCard.transform.localScale = new Vector3(1, 1, 1);
        Image CardImageComp = CurrentCard.GetComponent<Image>();
        CardImageComp.sprite = Resources.Load<Sprite>("Images/" + CardInfo.CardImageFile);
    }

    public void GoToNextCollectionPage()
    {
        if (!CardList.ContainsKey(CurrentPage + 1)) return;

        //Move out cards on current page
        foreach (GameObject Card in CardList[CurrentPage])
        {
            RectTransform ParentRect = CardCollectionArea.GetComponent<RectTransform>();
            Card.transform.position = new Vector2(ParentRect.rect.xMax + 1000.0f, ParentRect.rect.yMax + 1000.0f);
        }

        //Move in cards from next page
        ++CurrentPage;
        int cardIdx = 0;
        foreach (GameObject Card in CardList[CurrentPage])
        {
            Card.transform.position = CardsPlacementPositionList[cardIdx];
            ++cardIdx;
        }
    }

    public void GoToPreviousCollectionPage()
    {
        if (!CardList.ContainsKey(CurrentPage - 1)) return;

        //Move out cards on current page
        foreach (GameObject Card in CardList[CurrentPage])
        {
            RectTransform ParentRect = CardCollectionArea.GetComponent<RectTransform>();
            Card.transform.position = new Vector2(ParentRect.rect.xMax + 1000.0f, ParentRect.rect.yMax + 1000.0f);
        }

        //Move in cards from next page
        --CurrentPage;
        int cardIdx = 0;
        foreach (GameObject Card in CardList[CurrentPage])
        {
            Card.transform.position = CardsPlacementPositionList[cardIdx];
            ++cardIdx;
        }

    }

	// Update is called once per frame
	void Update () {

	}
}
