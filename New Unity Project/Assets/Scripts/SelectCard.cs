﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectCard : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
{
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () 
    {
	}

    public void OnPointerDown(PointerEventData eventData)
    {
    }

    public void OnPointerUp(PointerEventData eventData)
    {
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        GetComponent<DropStateBehaviour>().DeleteCardFromDropZone();
    }
}
