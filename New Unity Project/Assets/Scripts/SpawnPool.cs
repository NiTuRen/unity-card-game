﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Xml;
using System.Xml.Serialization;
using System.Reflection;

public class Spawn
{
    public Spawn()
    {
        UnitName = "";
        UnitID = 0;
        SpawnRating = 0;
        MinTurnsPassed = 0;
        Level = 0;
        Lane = "";
        SpawnedTurns = 0;
        SpawnedLane = 0;
    }

    public Spawn(Spawn c)
    {
        UnitName = c.UnitName;
        UnitID = c.UnitID;
        SpawnRating = c.SpawnRating;
        MinTurnsPassed = c.MinTurnsPassed;
        Level = c.Level;
        Lane = c.Lane;
        SpawnedTurns = c.SpawnedTurns;
        SpawnedLane = c.SpawnedLane;
    }


    public string UnitName { get; set; }
    public int UnitID { get; set; }
    public int SpawnRating { get; set; }
    public int MinTurnsPassed { get; set; }
    public int Level { get; set; }
    public string Lane { get; set; }
    public int SpawnedTurns;
    public int SpawnedLane;
};


public class SpawnPool
{
    public SpawnPool()
    {
        Class = "";
        SpawnList = new List<Spawn>();
    }

    public string Class  { get; set; }
    public List<Spawn> SpawnList { get; set; }  
};