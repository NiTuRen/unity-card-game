﻿using UnityEngine;

public class ManagerTile : Singleton<ManagerTile>
{
  private int[,] level;

  [SerializeField]
  public Transform holder;

  [SerializeField]
  private GameObject[] prefabs;

  private GameObject[,] tiles;

  public int sizeX { get { return 5; } }
  public int sizeY { get { return 3; } }

  public void Load()
  {
    tiles = new GameObject[sizeX, sizeY];
    for (int x = 0; x < sizeX; ++x)
    {
      for (int y = 0; y < sizeY; ++y)
      {
        // First prefab for general tiles
        // Second prefab for leftmost row (Player units spawnable)
        GameObject newTile = Instantiate(prefabs[x == 0 ? 1 : 0]);
        tiles[x, y] = newTile;
        newTile.GetComponent<TileBase>().Spawn(new Point(x, y));
      }
    }
  }

  public void Unload()
  {
    for (int x = 0; x < sizeX; ++x)
      for (int y = 0; y < sizeY; ++y)
        Destroy(tiles[x, y]);
  }
}
