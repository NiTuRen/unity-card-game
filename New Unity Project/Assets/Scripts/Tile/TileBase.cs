﻿using UnityEngine;
using System.Collections.Generic;

public class TileBase : MonoBehaviour
{
  public Point position { get; private set; }

  public void Spawn(Point pos)
  {
    position = pos;
    transform.position = position;
    transform.SetParent(ManagerTile.Instance.holder);
    name = "Tile " + position;
  }
}
