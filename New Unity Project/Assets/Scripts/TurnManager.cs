﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TurnManager : Singleton<TurnManager> {

    public enum GameState
    {
      Starting,
      Playing,
      Resolve,
      AI
    };

    public GameObject m_MainCanvas;
    public GameObject m_Camera;
    public int m_PlayerHp;
    public int m_BossHp;

    public GameObject UnitPrefab;

    public GameState state { get; private set; }

    private Text m_RoundText;
    private Button m_EndTurnButton;
    private int m_BossRoundNumber;
    private int m_RoundNumber;
    private WaitForSeconds m_WaitingTime;
    private bool m_EndTurn;
    private bool m_GameEnded;

    public List<GameObject> [,] m_BoardUnits;
    public List<int> m_PriorityUnits;
    public int g_UnitID;
    public List<GameObject> g_Units;    // all the units in the current level
    private Sprite[] FighterSprite;

    public int sizeX { get { return 5; } }
    public int sizeY { get { return 3; } }

	// Use this for initialization
	void Start () {
        //m_BoardUnits = new GameObject[sizeX, sizeY];
        m_BoardUnits = new List<GameObject>[sizeX,sizeY];

        for (int i = 0; i < sizeX; ++i)
        {
            for(int j = 0; j < sizeY; ++j)
            {
                m_BoardUnits[i, j] = new List<GameObject>();
            }
        }
        m_PriorityUnits = new List<int>();
        g_Units = new List<GameObject>();

        FighterSprite = Resources.LoadAll<Sprite>("Unit_Placeholder/Unit1");
        InitPlayingBoard();
	}

    void InitPlayingBoard()
    {
        m_WaitingTime = new WaitForSeconds(2);

        GameObject canvas = GameObject.Find("MessageCanvas"); // create UI components during ingame
        m_RoundText = canvas.GetComponentInChildren<Text>();
        m_EndTurnButton = canvas.GetComponentInChildren<Button>();
        m_EndTurnButton.onClick.AddListener(EndTurnTask);
        m_RoundText.text = "";

        ManagerTile.Instance.Load();
        m_Camera.GetComponent<Camera>().Load();

        StartCoroutine(GameLoop());
    }

    void EndTurnTask()
    {
        m_EndTurn = true;
    }

    void DisableControls()
    {
        m_EndTurnButton.gameObject.SetActive(false);
        m_EndTurnButton.enabled = false;
        
    }

    void EnableControls()
    {
        m_EndTurnButton.gameObject.SetActive(true);
        m_EndTurnButton.enabled = true;
    }

    private IEnumerator RoundStarting()
    {
        state = GameState.Starting;

        // disable controls
         DisableControls();

        // reset camera pos

        m_RoundNumber++;
        // increment round number and display
        m_RoundText.text = "ROUND " + m_RoundNumber.ToString();



        yield return m_WaitingTime;
    }

    private IEnumerator RoundPlaying()
    {
        state = GameState.Playing;

        // enable controls
        EnableControls();

        // draw a card


        while (!m_EndTurn)
        {
            m_RoundText.text = "PLAYER PLAYING";
            yield return null;
        }

        m_EndTurn = false;
    }

    private IEnumerator RoundResolve()
    {
        state = GameState.Resolve;

        // disable controls
        DisableControls();

        // move and resolve damage
        m_RoundText.text = "RESOLVE BOARD MECHANICS";
        ResolveBattle(true);

        // check any winners
        if (m_PlayerHp <= 0 || m_BossHp <= 0)
        {
            m_GameEnded = true;
            m_RoundText.text = "WINNER";
        }

        yield return m_WaitingTime;
    }

    private IEnumerator RoundAI()
    {
        state = GameState.AI;

        Spawn unitToSpawn = EnemyAI.Instance.ComputeEnemySpawn(true);
        // check through the cards collection and spawn
        foreach(Card element in GameManager.instance.m_Deck.CardsList)
        {
            if (element.CardID == unitToSpawn.UnitID)
            {
                GameObject newUnit = Instantiate(UnitPrefab);
                newUnit.GetComponent<Unit>().Spawn(element, new Point(4, unitToSpawn.SpawnedLane - 1), true, g_UnitID);
                if(element.CardID == 1)
                    newUnit.GetComponent<SpriteRenderer>().sprite = FighterSprite[0];
                else if(element.CardID == 2)
                    newUnit.GetComponent<SpriteRenderer>().sprite = FighterSprite[1];
                else
                    newUnit.GetComponent<SpriteRenderer>().sprite = FighterSprite[2];
                m_BoardUnits[4, unitToSpawn.SpawnedLane - 1].Add(newUnit);
                m_PriorityUnits.Add(g_UnitID++);    // assign a unit ID to each unit

                break;
            }
        }
        

        // AI algorithm
        m_RoundText.text = "AI PLAYING";
        
        yield return m_WaitingTime;
    }

    private IEnumerator GameLoop()
    {
        yield return StartCoroutine(RoundStarting());
        yield return StartCoroutine(RoundPlaying());
        yield return StartCoroutine(RoundResolve());
        yield return StartCoroutine(RoundAI());
        yield return StartCoroutine(RoundResolve());

        if (m_GameEnded)
        {
            // if there is a game winner, go to main menu
            SceneManager.LoadScene("MainMenu");
        }
        else
        {
            StartCoroutine(GameLoop());
        }
    }

    private void ResolveBattle(bool enemy)
    {
        foreach(int unitID in m_PriorityUnits)
        {
            g_Units[unitID].GetComponent<Unit>().Resolve();
        }
    }

    // @WOON, if you want to reference my cui code
    /*
    private bool CheckMovement(GameObject currentUnit, bool enemy)
    {
        Point currentUnitPos = currentUnit.GetComponent<Unit>().position;

        // change 1 to the number of steps the unit can move
        int left = enemy == true ? currentUnitPos.x - 1 : currentUnitPos.x + 1;
        int right = enemy == true ? currentUnitPos.x : left + 1;

        if(left >= 0 && left <= sizeX && right >= 0 && right <= sizeX)
        {
            for (int x = left; x < right; ++x)
            {
                GameObject unit = m_BoardUnits[x, currentUnitPos.y];
                // if there is a unit in front of the current unit
                if(unit && unit.GetComponent<Unit>().m_Alive)
                {
                    return false;
                }
            }
        }

        return true;
    }
    */
	
	// Update is called once per frame
	void Update () {

	}
}
