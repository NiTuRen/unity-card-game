﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour {

    Card m_UnitStats;

    public Point position { get; set; }
    public int m_UnitID;
    public bool m_Alive;


    public void Spawn(Card stats, Point pos, bool enemy, int unitID)
    {
        m_UnitStats = stats;
        position = pos;
        transform.position = position;
        name = "Unit " + position;

        if (enemy)
            tag = "EnemyUnit";
        else
            tag = "PlayerUnit";
        m_UnitID = unitID;
        TurnManager.Instance.g_Units.Add(gameObject); // add itself into the global unit
        m_Alive = true;
    }

	// Use this for initialization
	void Start () {
        
	}


    public void Resolve()
    {
        //@WOON TODO 
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
